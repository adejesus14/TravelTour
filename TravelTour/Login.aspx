﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TravelTour.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/alertify.core.css" rel="stylesheet" type="text/css" />
    <link href="css/alertify.default.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
   <%-- <link href="css/coverages-styles.css" rel="stylesheet" type="text/css" />--%>
    <link href="css/jquery-ui-1.11.3.css" rel="stylesheet" type="text/css" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.1.11.1.js" type="text/javascript"></script>
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <script src="js/global.js" type="text/javascript"></script>
    <script src="js/alertify.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.smartmenus.min.js" type="text/javascript"></script>
    <script src="js/jquery.smartmenus.bootstrap.min.js" type="text/javascript"></script>

    <%--<script type="text/javascript">
        history.pushState(null, null, '');
        window.addEventListener('popstate', function (event) {
            history.pushState(null, null, '');
        });

        function ValidateLogIn() {
            var txtUserName = document.getElementById('txtUserName').value.trim();
            var txtPassword = document.getElementById('txtPassword').value.trim();

            var error = 0;
            var msg = "";

            if (txtUser == "") {
                error = 1;
                msg += "<li>User</li>"
            }
            if (txtPassword == "") {
                error = 1;
                msg += "<li>Password</li>"
            }

            if (error != 0) {
                alertify.set({ delay: 10000 });
                alertify.error("Empty field(s):<br><ul>" + msg + "</ul>");
                return false;
            }
        }
    </script>--%>

    <script type="text/javascript">
        function ForgotPasswordShow() {
            $('#mdlForgotPassword').modal('show');
            $('#mdlForgotPassword').modal({
                backdrop: 'static',
                keyboard: false
            })
        }

        function ForgotPasswordHide() {
            $('#mdlForgotPassword').modal('hide');
            closeModalBackdrop();
        }

        function closeModalBackdrop() {
            $('.modal-backdrop').removeClass('modal-backdrop fade in');
        }
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>

        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="row text-center">
                        <img src="img/travellogin.png" class="animated fadeIn" style="margin-top: 230px; margin-bottom: -10px; height: 90px" />
                        <br />
                        <h4 style="margin-bottom: 20px;" class="animated fadeInLeft">Login Account</h4>

                        <div class="col-md-offset-3 col-md-6">
                            <div class="panel panel-default animated fadeInRight">
                                <div class="panel-body">
                                    <div class="row" style="margin-top: 20px;">
                                        <div class="form-horizontal">
                                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group">   
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                        <asp:TextBox ID="txtUserId" MaxLength="20" runat="server" CssClass="form-control" Placeholder="User ID" Width="100%" autocomplete="off" TabIndex="1"></asp:TextBox>
                                                    </div>
                                                 </div>
                                                <br />
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                        <asp:TextBox ID="txtPassword" MaxLength="20" runat="server" CssClass="form-control" Placeholder="Password" Width="100%" autocomplete="off" TabIndex="1" TextMode="Password"></asp:TextBox> 
                                                    </div>
                                                    <div class="row text-center" style="margin-top: 10px;">
                                     <asp:Button ID="btnLogin" runat="server" Text="Sign In" CssClass="btn btn-primary"  UseSubmitBehavior="true" /> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        
                                    </div>
                                    <div class="row text-center">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-danger">
                                <asp:Label ID="lblAlertMsg" runat="server" Visible="false">Your Session is Expired!</asp:Label>
                            </div>
                            <br />
                            <div class="page-footer animated fadeInUp">
                                <span class="text-center">Copyright © 2016 PayorPortal. All Rights Reserved.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

    </form>
</body>
</html>
