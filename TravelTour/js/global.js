﻿function ConfirmDelete() {
    var message = "Are you sure you want to delete?";
    return confirm(message);
}

//function CloseModal(_modalName) {
//    $find(_modalName).hide();
//}

function SetCursorToTextEnd(textControlID) {
    var text = document.getElementById(textControlID);
    if (text != null && text.value.length > 0) {
        if (text.createTextRange) {
            var FieldRange = text.createTextRange();
            FieldRange.moveStart('character', text.value.length);
            FieldRange.collapse();
            FieldRange.select();
        } else if (text.setSelectionRange) {
            var textLength = text.value.length;
            text.setSelectionRange(textLength, textLength);
        }
    }
}

function PostBackTextBox(TextBoxID) {
    __doPostBack(TextBoxID, '');
};

function SetEnd(TB) {
    if (TB.createTextRange) {
        var FieldRange = TB.createTextRange();
        FieldRange.moveStart('character', TB.value.length);
        FieldRange.collapse();
        FieldRange.select();
    }
}

function OpenModal(modal) {
    $(modal).modal('show');
    //$(modal).modal({
    //    backdrop: 'static',
    //    keyboard: false
    //});
}

function CloseModal(modal) {
    $(modal).modal('hide');
    $('.modal-backdrop').removeClass('modal-backdrop fade in');
    //closeModalBackdrop();
    //$('body').removeClass('modal-open');
    //$('.modal-backdrop').remove();
}

function closeModalBackdrop() {
    $('.modal-backdrop').removeClass('modal-backdrop fade in');
}

function IsDate(val) {
    var d = new Date(val);
    return !isNaN(d.valueOf());
}

function CheckAmount(evt) {
    var keyCode = evt.which ? evt.which : evt.keyCode;
    //if (window.event.keyCode==13 || window.event.keyCode==46){return true;} 
    if (keyCode == 8)
    { return true; }
    if (keyCode == 46)
    { return true; }
    if ((keyCode >= 48) && (keyCode <= 57))
    { return true; }
    else
    { return false; }
}

function CheckAmountwithNegative(evt) {
    var keyCode = evt.which ? evt.which : evt.keyCode;
    //if (window.event.keyCode==13 || window.event.keyCode==46){return true;} 
    if (keyCode == 8)
    { return true; }
    if (keyCode == 46)
    { return true; }
    if ((keyCode >= 48) && (keyCode <= 57))
    { return true; }
    if ((keyCode == 189) || (keyCode == 109) || (keyCode == 45))
    { return true; }
    else
    { return false; }
}

function IsNumeric(input) {
    return (input - 0) == input && ('' + input).trim().length > 0;
}

function strTrim(x) {
    return x.replace(/^\s+|\s+$/gm, '');
}

//function parseDate(str) {
//    var mdy = str.split('/')
//    return new Date(mdy[2], mdy[0] - 1, mdy[1]);
//}

//function daydiff(first, second) {
//    return (second - first) / (1000 * 60 * 60 * 24);
//}

//Case Insensitive contains function
jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function (arg) {
    return function (elem) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

//Filter table with given text
function FilterTable(filterText, filterTable) {
    $('input[type=text][id*=' + filterText + ']').keyup(function () {
        var text = $(this).val();
        $("table[id*=" + filterTable + "] tr").show();
        if (text.length != 0) {
            $("table[id*=" + filterTable + "] tr:has('td')").each(function () {
                if ($(this).find("td:Contains(" + text + ")").length == 0) {
                    $(this).hide();
                }
            });
        }
    });
}

function padZeroDate(num) {
    return ((num <= 9) ? ("0" + num) : num);
}

function openCloseCollapse(id, expandGlyphicon, collapseGlyphicon) {
    if ($('#' + id).hasClass('hidden')) {
        $('#' + id).fadeIn().removeClass('hidden');
        $('#span' + id).removeClass(collapseGlyphicon).addClass(expandGlyphicon);
    }
    else {
        $('#' + id).delay(200).fadeOut(0, function () { $(this).addClass('hidden'); });
        $('#span' + id).removeClass(expandGlyphicon).addClass(collapseGlyphicon);
    }
}

function ClickEvent(id) {
    document.getElementById(id).click();
}

function doEnterClickEvent(id, e) {
    var key;

    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //firefox

    if (key == 13) {
        var btn = document.getElementById(id);
        if (btn != null) {
            event.keyCode = 0
            btn.click();
        }
    }
}