﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TravelTour.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="TravelTour.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <html>

    <head>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
    </head>

    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
        <style>
            .navbar-default {
                background-color: #3097d1;
            }

            #leftPanel {
                text-align: center;
            }

            .img-circle {
                width: 100px;
                height: 100px;
                overflow: hidden;
            }
            /* TEST */
            .panel {
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                margin-bottom: 30px;
                margin-top: 10px;
            }

                .panel.panel-default {
                    border: 1px solid #d4d4d4;
                    -webkit-box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.1);
                    -moz-box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.1);
                    box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.1);
                }

            .page1 {
                border: 1px solid #d4d4d4;
                -webkit-box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.1);
                -moz-box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.1);
                box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.1);
            }

            .btn-icon {
                color: #484848;
            }

            .cliente {
                margin-top: 10px;
                border: #cdcdcd thin solid;
                border-radius: 5px;
                -moz-border-radius: 0px;
                -webkit-border-radius: 0px;
            }

            .cliente-img {
                margin-top: 20px;
                margin-bottom: 20px;
                text-align: center;
            }

            .cliente-txt {
                font-family: Arial;
                font-size: 14px;
                margin-top: 20px;
                margin-bottom: 20px;
                margin-left: 10px;
                margin-right: 10px;
                text-align: left;
            }
            /*font Awesome http://fontawesome.io*/
            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
            /*Comment List styles*/
            .comment-list .row {
                margin-bottom: 0px;
            }

            .comment-list .panel .panel-heading {
                padding: 4px 15px;
                position: absolute;
                border: none;
                /*Panel-heading border radius*/
                border-top-right-radius: 0px;
                top: 1px;
            }

                .comment-list .panel .panel-heading.right {
                    border-right-width: 0px;
                    /*Panel-heading border radius*/
                    border-top-left-radius: 0px;
                    right: 16px;
                }

                .comment-list .panel .panel-heading .panel-body {
                    padding-top: 6px;
                }

            .comment-list figcaption {
                /*For wrapping text in thumbnail*/
                word-wrap: break-word;
            }
            /* Portrait tablets and medium desktops */
            @media (min-width: 768px) {
                .comment-list .arrow:after, .comment-list .arrow:before {
                    content: "";
                    position: absolute;
                    width: 0;
                    height: 0;
                    border-style: solid;
                    border-color: transparent;
                }

                .comment-list .panel.arrow.left:after, .comment-list .panel.arrow.left:before {
                    border-left: 0;
                }
                /*****Left Arrow*****/
                /*Outline effect style*/
                .comment-list .panel.arrow.left:before {
                    left: 0px;
                    top: 30px;
                    /*Use boarder color of panel*/
                    border-right-color: inherit;
                    border-width: 16px;
                }
                /*Background color effect*/
                .comment-list .panel.arrow.left:after {
                    left: 1px;
                    top: 31px;
                    /*Change for different outline color*/
                    border-right-color: #FFFFFF;
                    border-width: 15px;
                }
                /*****Right Arrow*****/
                /*Outline effect style*/
                .comment-list .panel.arrow.right:before {
                    right: -16px;
                    top: 30px;
                    /*Use boarder color of panel*/
                    border-left-color: inherit;
                    border-width: 16px;
                }
                /*Background color effect*/
                .comment-list .panel.arrow.right:after {
                    right: -14px;
                    top: 31px;
                    /*Change for different outline color*/
                    border-left-color: #FFFFFF;
                    border-width: 15px;
                }
            }

            .comment-list .comment-post {
                margin-top: 6px;
            }

            /* END TEST*/
        </style>

        <%--<nav class="navbar navbar-default navbar-fixed-top">--%>
        <header id="header">
            <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
            <nav class="navbar navbar-default navbar-fixed-top menu">


                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index-register.html">
                        <img src="img/test.png" alt="logo" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left main-menu">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span></span></a>

                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Newsfeed <span></span></a>

                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Timeline <span></span></a>

                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle pages" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">All Pages <span></span></a>
                        </li>
                        <li class="dropdown"><a href="contact.html">Contact</a></li>
                        

                    </ul>
                    <%-- <form class="navbar-form navbar-right hidden-sm">
              <div class="form-group">
                <i class="icon ion-android-search"></i>
                <input type="text" class="form-control" placeholder="Search friends, photos, videos">
              </div>
            </form>--%>
                </div>
                <!-- /.navbar-collapse -->
                <!-- /.container -->
            </nav>
        </header>
        <div id="page content">
            <div class="container">
                <div class="row">

                    <br />
                    <div class="col-md-2 static" id="page1">
                        <div class="cliente">
                            <div class="cliente-img">
                                <img src="img/morty.png" alt="Texto Alternativo" class="img-circle" />
                                <h2>Harold Viduya</h2>
                                <p>An aspiring Programmer, that has a passion for change.</p>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning">
                                        Social</button>
                                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span><span class="sr-only">Social</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="https://plus.google.com/+Jquery2dotnet/posts">Google +</a></li>
                                        <li><a href="https://www.facebook.com/jquery2dotnet">Facebook</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Github</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cliente">
                            <div class="cliente-txt">
                                <b>About</b> - <a href="#">edit</a>

                                <div class="form-group">
                                    <span class="glyphicon glyphicon-home"></span>Lives in <a href="#">Taguig City,Philippines</a>
                                </div>
                                <div class="form-group">
                                    <span class="glyphicon glyphicon-folder-open"></span>Works at <a href="#">Medilink Network Inc</a>
                                </div>
                                <div class="form-group">
                                    <span class="glyphicon glyphicon-book"></span>Studied at <a href="#">Global City Innovative College</a>
                                </div>
                                <div class="form-group">
                                    <span class="glyphicon glyphicon-user"></span>Followers <a href="#">1200</a>
                                </div>
                            </div>
                        </div>
                        <div class="cliente">
                            <div class="cliente-txt">
                                <b>Photos</b> - <a href="#">edit</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7 static">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <input type="email" class="form-control" placeholder="What are you up to?" />
                                <br />

                            </div>
                            <div class="panel-footer">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-success">Post</button>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <h2 class="page-header">What's Up?</h2>
                                    <section class="comment-list">
                                        <!-- First Comment -->
                                        <article class="row">
                                            <div class="col-md-2 col-sm-2 hidden-xs">
                                                <figure class="thumbnail">
                                                    <img class="img-responsive" src="img/igop.jpg" />
                                                    <figcaption class="text-center"><b>ECNALUBMA</b></figcaption>
                                                </figure>
                                            </div>
                                            <div class="col-md-10 col-sm-10">
                                                <div class="panel panel-default arrow left">
                                                    <div class="panel-body">
                                                        <header class="text-left">
                                                            <%--<div class="comment-user"><i class="fa fa-user"></i>That Guy</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>Dec 16, 2014</time>--%>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                                Pare ang pogi mo ah
                   
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i> reply</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Second Comment Reply -->
                                        <article class="row">
                                            <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                                                <figure class="thumbnail">
                                                    <img class="img-responsive" src="img/morty.png" />
                                                    <figcaption class="text-center">Harold</figcaption>
                                                </figure>
                                            </div>
                                            <div class="col-md-9 col-sm-9">
                                                <div class="panel panel-default arrow left">
                                                    <div class="panel-heading right">Reply</div>
                                                    <div class="panel-body">
                                                        <header class="text-left">
                                                            <%--<div class="comment-user"><i class="fa fa-user"></i>That Guy</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>Dec 16, 2014</time>--%>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                              Salamat pre alam ko
                   
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i> reply</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Third Comment -->
                                        <article class="row">
                                            <div class="col-md-10 col-sm-10">
                                                <div class="panel panel-default arrow right">
                                                    <div class="panel-body">
                                                        <header class="text-right">
                                                            <div class="comment-user"><i class="fa fa-user"></i>That Guy</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>Dec 16, 2014</time>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                   
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i> reply</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 hidden-xs">
                                                <figure class="thumbnail">
                                                    <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                                                    <figcaption class="text-center">username</figcaption>
                                                </figure>
                                            </div>
                                        </article>
                                        <!-- Fourth Comment -->
                                        <article class="row">
                                            <div class="col-md-2 col-sm-2 hidden-xs">
                                                <figure class="thumbnail">
                                                    <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                                                    <figcaption class="text-center">username</figcaption>
                                                </figure>
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <div class="panel panel-default arrow left">
                                                    <div class="panel-body">
                                                        <header class="text-left">
                                                            <div class="comment-user"><i class="fa fa-user"></i>That Guy</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>Dec 16, 2014</time>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                   
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i> reply</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Fifth Comment -->
                                        <article class="row">
                                            <div class="col-md-10 col-sm-10">
                                                <div class="panel panel-default arrow right">
                                                    <div class="panel-body">
                                                        <header class="text-right">
                                                            <div class="comment-user"><i class="fa fa-user"></i>That Guy</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>Dec 16, 2014</time>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                   
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i> reply</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 hidden-xs">
                                                <figure class="thumbnail">
                                                    <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                                                    <figcaption class="text-center">username</figcaption>
                                                </figure>
                                            </div>
                                        </article>
                                        <!-- Sixth Comment Reply -->
                                        <article class="row">
                                            <div class="col-md-9 col-sm-9 col-md-offset-1 col-md-pull-1 col-sm-offset-0">
                                                <div class="panel panel-default arrow right">
                                                    <div class="panel-heading">Reply</div>
                                                    <div class="panel-body">
                                                        <header class="text-right">
                                                            <div class="comment-user"><i class="fa fa-user"></i>That Guy</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>Dec 16, 2014</time>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                   
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i> reply</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-md-pull-1 hidden-xs">
                                                <figure class="thumbnail">
                                                    <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                                                    <figcaption class="text-center">username</figcaption>
                                                </figure>
                                            </div>
                                        </article>
                                    </section>
                                </div>
                            </div>
                        </div>
              
            </div>
            <div class="col-md-2 static">
                <div>
                    <img src="http://lorempixel.com/200/200/abstract/1/" alt="Texto Alternativo" class="img-circle" />
                    <h2>Harold Viduya</h2>
                    <p>An aspiring Programmer, that has a passion for change.</p>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning">
                            Social</button>
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span><span class="sr-only">Follow</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Twitter</a></li>
                            <li><a href="https://plus.google.com/+Jquery2dotnet/posts">Google +</a></li>
                            <li><a href="https://www.facebook.com/jquery2dotnet">Facebook</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Github</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        </div>
        </div>




    </body>
    </html>
</asp:Content>
